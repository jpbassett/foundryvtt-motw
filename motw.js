import { MotwActorSheet } from "./modules/actor-sheet.js";
import { MotwActor } from "./modules/entity.js";
import { MotwItemSheet } from "./modules/item-sheet.js";
import { MOTW } from "./modules/config.js";

Handlebars.registerHelper("subtract", function (a, b) {
  return a - b;
});

Handlebars.registerHelper("multiply", function (a, b) {
  return a * b;
});

Handlebars.registerHelper("eq", function (a, b) {
  return a == b;
});

Hooks.once("init", async function () {
  CONFIG.MOTW = MOTW;
  CONFIG.Actor.entityClass = MotwActor;
  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("motw", MotwActorSheet, {
    types: ["hunter", "npc"],
    makeDefault: true,
  });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("motw", MotwItemSheet, {
    types: ["item", "move"],
    makeDefault: true,
  });
});

Hooks.once("setup", function () {
  // Localize CONFIG objects once up-front
  const toLocalize = ["attributes"];
  for (let o of toLocalize) {
    CONFIG.MOTW[o] = Object.entries(CONFIG.MOTW[o]).reduce((obj, e) => {
      obj[e[0]] = game.i18n.localize(e[1]);
      return obj;
    }, {});
  }
});
